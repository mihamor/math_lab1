﻿using System;
namespace Math_lab1
{
    public class SimpleIteration : RefinmentMethod
    {
        readonly MathFunction compressionFunction;
        readonly double q;
        public SimpleIteration(
            DifferentiableFunction function,
            double leftBorder,
            double rightBorder,
            double accuracy
        ) : base(function, leftBorder, rightBorder, accuracy)
        {

            double alpha = Math.Min(
                function.GetFirstDerivativeValue(leftBorder),
                function.GetFirstDerivativeValue(rightBorder)
                );
            double gamma = Math.Max(
                function.GetFirstDerivativeValue(leftBorder),
                function.GetFirstDerivativeValue(rightBorder)
                );
            double lambda = 2 / (alpha + gamma);
            Console.WriteLine(lambda);
            q = (gamma - alpha) / (gamma + alpha);
            compressionFunction = function.GetCompressionFunction(lambda);
        }

        public override double Solve()
        {
            int k = 0;
            double x, currAccuracy;
            double xPrevious = rightBorder;

            do
            {
                x = compressionFunction.GetValue(xPrevious);
                interRoots.Add(xPrevious);
                k += 1;
                Console.WriteLine("{0} {1}", k, x);
                currAccuracy = Math.Abs(xPrevious - x);
                xPrevious = x;
            } while (currAccuracy > Math.Abs((1 - q) * accuracy / q));
            return x;
        }
    }
}

