﻿using System;
using System.Collections.Generic;

namespace Math_lab1
{
    public class Controller
    {
        readonly View view;
        readonly MathFunction func1 = new MathFunction(
            x => Math.Sinh(x) * Math.Sin(7 * x) - 5 * Math.Exp(x) * Math.Cos(x)
        );
        readonly DifferentiableFunction func2 = new DifferentiableFunction(
            x => Math.Exp(Math.Sin(x) + Math.Cos(x) + 1) - 3 * Math.Log(x),
            x => Math.Exp(Math.Sin(x) + Math.Cos(x) + 1) * (Math.Cos(x) - Math.Sin(x)) - (3 / x),
            x => Math.Exp(Math.Sin(x) + Math.Cos(x) + 1) * Math.Pow(Math.Cos(x) - Math.Sin(x), 2)
                - Math.Exp(Math.Sin(x) + Math.Cos(x) + 1) * (Math.Sin(x) + Math.Cos(x))
                + (3 / Math.Pow(x, 2))
        );


    public Controller(View view)
        {
            this.view = view;
        }

        public void Start()
        {
            bool running = true;

            while (running)
            {
                MethodType type = view.AskForMethodType();
                double epsilon = view.AskForEpsilon();

                IMethod method = SelectMethod(type, epsilon);

                try
                {
                    if(type == MethodType.Lobachevksiy)
                    {
                        LobachevskiyMethod lobachevskiyMethod = (LobachevskiyMethod)method;
                        List<double> roots = lobachevskiyMethod.SolveAll();
                        double achievedAccuracy = lobachevskiyMethod.AchievedAccuracy;
                        roots.Sort();
                        view.ShowRoots(roots, achievedAccuracy);
                        double step = achievedAccuracy / 2;

                        List<double> exactRoots = new List<double>();
                        foreach (double root in roots)
                        {
                            BisectionMethod bisectionMethod = new BisectionMethod(
                                lobachevskiyMethod.Function, root - step, root + step, epsilon
                            );
                            double exactRoot = bisectionMethod.Solve();
                            exactRoots.Add(exactRoot);
                        }

                        view.ShowRoots(exactRoots, epsilon);
                    }
                    else
                    {
                        double root = method.Solve();
                        view.ShowRoots(((RefinmentMethod)method).GetInterRoots(), epsilon);
                        view.ShowRoot(root, epsilon);
                    }

                }
                catch (Exception e)
                {
                    Console.WriteLine("Can't solve: {0}", e.Message);
                }
                running = view.AskForExit();
            }
        }


        IMethod SelectMethod(
            MethodType type,
            double epsilon
            )
        {
            switch (type)
            {
                case MethodType.Bisection:
                    {
                        double leftBorder = view.AskForLeftBorder();
                        double rightBorder = view.AskForRightBorder();
                        BisectionMethod bisectionMethod = new BisectionMethod(func1, leftBorder, rightBorder, epsilon);
                        bisectionMethod.AddIterationListener(new EventHandler<MethodEventArgs>(Logger.GetLogger()));
                        return bisectionMethod;
                    }
                case MethodType.NewtonSimplified:
                    {
                        double leftBorder = view.AskForLeftBorder();
                        double rightBorder = view.AskForRightBorder();
                        NewtonMethod newtonMethod = new NewtonMethod(func2, leftBorder, rightBorder, epsilon);
                        newtonMethod.AddIterationListener(
                            new EventHandler<NewtonMethodEventArgs>(Logger.GetNewtonLogger())
                        );
                        return newtonMethod;
                    }
                case MethodType.SimpleIteration:
                    {
                        double leftBorder = view.AskForLeftBorder();
                        double rightBorder = view.AskForRightBorder();
                        SimpleIteration iterationMethod = new SimpleIteration(func2, leftBorder, rightBorder, epsilon);
                        return iterationMethod;
                    }
                case MethodType.Lobachevksiy:
                    {
                        // 150x^7 + 249x^6 - 661x^5 - 905x^4 + 885x^3 + 917x^2 - 290x - 256
                        List<double> coefficients = new List<double>();
                        
                        coefficients.AddRange(new double[] { 150, 249, -661, -905, 885, 917, -290, -256 });
                        AlgebraicFunction function = AlgebraicFunction.FromCoefficients(coefficients);
                        LobachevskiyMethod lobachevskiyMethod = new LobachevskiyMethod(function, epsilon);
                        return lobachevskiyMethod;
                    }
            }
            return null;
        }
    }
}
