﻿using System;
namespace Math_lab1
{

    public class MethodEventArgs : EventArgs
    {

        public double FuncValueRight { get; set; }
        public double FuncValueLeft { get; set; }
        public double FuncValueCenter { get; set; }
        public double Center { get; set; }
        public double Right { get; set; }
        public double Left { get; set; }
        public int Iteration { get; set; }

        public MethodEventArgs(
            double leftBorder,
            double rightBorder,
            double center,
            double f_right,
            double f_left,
            double f_center,
            int iteration
            )
        {
            FuncValueCenter = f_center;
            FuncValueLeft = f_left;
            FuncValueRight = f_right;
            Right = rightBorder;
            Left = leftBorder;
            Center = center;
            Iteration = iteration;
        }
    }

    public class NewtonMethodEventArgs : EventArgs
    {
        public double FuncValue { get; set; }
        public double Value { get; set; }
        public int Iteration { get; set; }
        public NewtonMethodEventArgs(
            double x,
            double funcX,
            int iteration
            )
        {
            FuncValue = funcX;
            Value = x;
            Iteration = iteration;
        }
    }

    public static class Logger
    {
        public static EventHandler<MethodEventArgs> GetLogger()
        {
            return (object sender, MethodEventArgs e) =>
             {
                 Console.WriteLine("Iteration: {0}", e.Iteration);
                 Console.WriteLine("({0}, {1})", e.Left, e.Right);
                 Console.WriteLine("F({0}) = {1}\nF({2}) = {3}",
                        e.Left, e.FuncValueLeft, e.Right, e.FuncValueRight);
             };
        }

        public static EventHandler<NewtonMethodEventArgs> GetNewtonLogger()
        {
            return (object sender, NewtonMethodEventArgs e) =>
            {
                Console.WriteLine("Iteration: {0}", e.Iteration);
                Console.WriteLine("(x = {0}, F(x) = {1})", e.Value, e.FuncValue);
            };
        }
    }
}
