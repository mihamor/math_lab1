﻿using System;
namespace Math_lab1
{
    public class BisectionMethod : RefinmentMethod
    {
        public BisectionMethod(
            MathFunction function,
            double leftBorder,
            double rightBorder,
            double accuracy
        ) : base(function, leftBorder, rightBorder, accuracy) { }

        public override double Solve()
        {
            double center;
            int k = 0;
            do
            {
                center = (rightBorder + leftBorder) / 2;
                interRoots.Add(center);
                double f_right = function.GetValue(rightBorder);
                double f_left = function.GetValue(leftBorder);
                double f_center = function.GetValue(center);
                if (Math.Sign(f_left * f_center) < 0)
                    rightBorder = center;
                else leftBorder = center;
                k += 1;

                MethodEventArgs args = new MethodEventArgs(
                    leftBorder, rightBorder, center, f_right, f_left, f_center, k);
                OnIteration(args);

            } while (Math.Abs(rightBorder - leftBorder) > accuracy);
            return center;
        }
    }
}
