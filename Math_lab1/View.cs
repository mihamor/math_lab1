﻿using System;
using System.Collections.Generic;

namespace Math_lab1
{

    public class View
    {

        public MethodType AskForMethodType()
        {
            Console.Clear();
            var values = Enum.GetValues(typeof(MethodType));
            for(int index = 0; index < values.Length; index++)
            {
                string method = values.GetValue(index).ToString();
                Console.WriteLine("{0}) {1} method", index + 1, method);
            }
            int choice = Convert.ToUInt16(Console.ReadLine());
            return (MethodType)values.GetValue(choice - 1);
        }

        public double AskForLeftBorder()
        {
            Console.Clear();
            Console.WriteLine("Enter left border:");
            return Convert.ToDouble(Console.ReadLine());
        }

        public double AskForRightBorder()
        {
            Console.Clear();
            Console.WriteLine("Enter Right border:");
            return Convert.ToDouble(Console.ReadLine());
        }

        public double AskForEpsilon()
        {
            Console.Clear();
            Console.WriteLine("Enter accuracy order(10^-(order):");
            int order = Convert.ToUInt16(Console.ReadLine());
            return Math.Pow(10, -(order));
        }

        public void ShowRoot(double root, double accuracy)
        {
            Console.WriteLine("Root: {0}, Accuracy: {1}", root, accuracy);
        }

        public void ShowRoots(List<double> roots, double accuracy)
        {
            string rootsString = "{ ";
            foreach(double root in roots)
            {
                rootsString += root + ", ";
            }
            rootsString += " }";
            Console.WriteLine("Roots: {0}, Accuracy: {1}", rootsString, accuracy);
        }

        public bool AskForExit()
        {
            Console.WriteLine("0) Exit");
            Console.WriteLine("1) Continue");
            return Convert.ToBoolean(Convert.ToUInt16(Console.ReadLine()));
        }
    }
}
