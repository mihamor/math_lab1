﻿using System;
namespace Math_lab1
{
    public class NewtonMethod : RefinmentMethod
    {
        protected event EventHandler<NewtonMethodEventArgs> NewtonIterationEvent;

        public NewtonMethod(
             DifferentiableFunction function,
             double leftBorder,
             double rightBorder,
             double accuracy
         ) : base(function, leftBorder, rightBorder, accuracy) {}


        private bool FourierCondition(double x)
        {
            DifferentiableFunction function = (DifferentiableFunction)this.function;
            return Math.Sign(function.GetValue(x) * function.GetSecondDerivativeValue(x)) > 0;
        }

        private bool IntervalValidity()
        {
           return Math.Sign(function.GetValue(leftBorder) * function.GetValue(rightBorder)) < 0;
        }

        private double GetStartingValue()
        {

            bool rightFourier = FourierCondition(rightBorder);
            bool leftFourier = FourierCondition(leftBorder);

            if (rightFourier) return rightBorder;
            if (leftFourier) return leftBorder;

            double start = leftBorder;

            for (;start < rightBorder; start += 0.01)
            {
                if (FourierCondition(start))
                {
                    return start;
                }
            }
            throw new Exception("Cant find start");
        }

        public override double Solve()
        {
            DifferentiableFunction function = (DifferentiableFunction)this.function;
            if(!IntervalValidity())
            {
                throw new Exception("Invalid interval");
            }

            double x = GetStartingValue();
            double previousX = x;
            double stepFactor = 1 / function.GetFirstDerivativeValue(x);
            int iteration = 0;

            while (Math.Abs(function.GetValue(x)) > accuracy)
            {
                double func_x = function.GetValue(previousX);

                NewtonMethodEventArgs args = new NewtonMethodEventArgs(previousX, func_x, iteration);
                OnIteration(args);


                interRoots.Add(x);
                x = previousX - (func_x * stepFactor);
                previousX = x;

                iteration += 1;
            }

            return x;
        }

        protected virtual void OnIteration(NewtonMethodEventArgs e)
        {
            NewtonIterationEvent.Invoke(this, e);
        }

        public void AddIterationListener(EventHandler<NewtonMethodEventArgs> eventHandler)
        {
            NewtonIterationEvent += eventHandler;
        }
    }
}
