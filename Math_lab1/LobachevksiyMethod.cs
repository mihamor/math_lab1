﻿using System;
using System.Collections.Generic;


// 150x^7 + 249x^6 - 661x^5 - 905x^4 + 885x^3 + 917x^2 - 290x - 256
namespace Math_lab1
{
    public class LobachevskiyMethod : IMethod
    {
        private List<double> currentCoefficient;
        private List<double> previousCoefficient;
        private readonly List<double> roots;
        private readonly double accuracy;

        public int Iteration { get; set; }
        public double AchievedAccuracy { get; set; }

        public AlgebraicFunction Function { get; }

        public LobachevskiyMethod(AlgebraicFunction function, double accuracy)
        {
            Function = function;
            this.accuracy = accuracy;
            roots = new List<double>();
            currentCoefficient = function.Coefficients;
        }

        public double Solve()
        {
            return SolveAll()[0];
        }

        public List<double> SolveAll()
        {
            Iteration = 0;
            do
            {
                previousCoefficient = currentCoefficient;
                List<double> nextCoefficient = new List<double>();
                for (int i = 0; i < currentCoefficient.Count; i++)
                {
                    double a = 0;
                    for (int k = 0; i - k >= 0 && i + k < currentCoefficient.Count; k++)
                    {
                        int multiplier = k == 0 ? 1 : 2;
                        
                        a += Math.Pow(-1, k) * multiplier * currentCoefficient[i - k] * currentCoefficient[i + k];
                        if (double.IsInfinity(a)) return roots;
                    }
                    nextCoefficient.Add(a);
                }
                currentCoefficient = nextCoefficient;
                Iteration += 1;

                roots.Clear();
                for (int i = 0; i < currentCoefficient.Count - 1; i++)
                {
                    double root = -Math.Pow(currentCoefficient[i + 1] / currentCoefficient[i], Math.Pow(2, -Iteration));
                    roots.Add(root);
                    if (Math.Abs(Function.GetValue(roots[i])) > Math.Abs(Function.GetValue(-roots[i])))
                    {
                        roots[i] *= -1;
                    }
                }
            }
            while (!ShouldStop());
            return roots;
        }

        private bool ShouldStop()
        {
            AchievedAccuracy = 0;
            for (int i = 0; i < currentCoefficient.Count; i++)
            {
                AchievedAccuracy += Math.Pow(1 - currentCoefficient[i] / Math.Pow(previousCoefficient[i], 2), 2);
            }
            AchievedAccuracy = Math.Pow(AchievedAccuracy, 0.5);
            return AchievedAccuracy < accuracy;
        }
    }
}
