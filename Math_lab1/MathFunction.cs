﻿using System;
using System.Collections.Generic;

namespace Math_lab1
{
    public class MathFunction
    {
        protected readonly Func<double, double> expression;
        public MathFunction(Func<double, double> expression)
        {
            this.expression = expression;
        }
        public virtual double GetValue(double x)
        {
            return expression(x);
        }
        public MathFunction GetCompressionFunction(double lambda)
        {
            return new MathFunction(
                x => x - lambda * GetValue(x)
            );
        }
    }

    public class DifferentiableFunction : MathFunction
    {
        readonly Func<double, double> firstDerivative;
        readonly Func<double, double> secondDerivative;

        public DifferentiableFunction(
            Func<double, double> expression,
            Func<double, double> firstDerivative,
            Func<double, double> secondDerivative
        )
            : base(expression)
        {
            this.firstDerivative = firstDerivative;
            this.secondDerivative = secondDerivative;
        }

        public double GetFirstDerivativeValue(double x)
        {
            return firstDerivative(x);
        }

        public double GetSecondDerivativeValue(double x)
        {
            return secondDerivative(x);
        }
    }

    public class AlgebraicFunction : MathFunction
    {
        public AlgebraicFunction(Func<double, double> expression,
            List<double> coefficients
            ) : base(expression)
        {
            Coefficients = coefficients;
        }

        public static AlgebraicFunction FromCoefficients(List<double> coefficients)
        {
            double expression(double x)
            {
                double result = 0;
                for (int i = 0; i < coefficients.Count; i++)
                {
                    result += coefficients[i] * Math.Pow(x, coefficients.Count - i - 1);
                }
                return result;
            }

            return new AlgebraicFunction(expression, coefficients);
        }

        public List<double> Coefficients { get; set; }
    }
}
