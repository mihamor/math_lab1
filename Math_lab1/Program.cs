﻿using System;

namespace Math_lab1
{
    class MainClass
    {
        // bisection
        // - 2.7 to - 2.4
        // -2.3 to -2.1
        // -1.9 to -1.6
        // -1.4 to 1.2
        // -1.1 to -0.9
        // 1.5 to 1.7

        // newtone
        // 2.3 to 2.4
        // 5.9 to 6.1
        // 7.9 to 8.1
        public static void Main(string[] args)
        {
            View view = new View();
            Controller controller = new Controller(view);

            controller.Start();
        }
    }
}
