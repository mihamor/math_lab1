﻿using System;
using System.Collections.Generic;

namespace Math_lab1
{
    public enum MethodType
    {
        Bisection,
        NewtonSimplified,
        SimpleIteration,
        Lobachevksiy
    };

    public interface IMethod
    {
        double Solve();
    }

    public abstract class RefinmentMethod : IMethod
    {

        protected event EventHandler<MethodEventArgs> IterationEvent;
        protected readonly MathFunction function;
        protected readonly double accuracy;
        public List<double> interRoots;
        protected double rightBorder;
        protected double leftBorder;

        protected RefinmentMethod(MathFunction function, double leftBorder, double rightBorder, double accuracy)
        {
            this.function = function;
            this.rightBorder = rightBorder;
            this.leftBorder = leftBorder;
            this.accuracy = accuracy;
            interRoots = new List<double>();
        }

        public abstract double Solve();
        protected virtual void OnIteration(MethodEventArgs e)
        {
            IterationEvent?.Invoke(this, e);
        }
        public void AddIterationListener(EventHandler<MethodEventArgs> eventHandler)
        {
            IterationEvent += eventHandler;
        }

        public List<double> GetInterRoots()
        {
            return interRoots;
        }
    }
}
